SELECT *FROM book;

SELECT *FROM profile;

SELECT name, price
FROM book;

SELECT username, gender, birthday
FROM profile;

SELECT DISTINCT username, email
FROM profile;

SELECT DISTINCT id, date, status
FROM orders;

SELECT username, birthday
FROM profile
WHERE gender='female';

SELECT username, gender, email
FROM profile
WHERE email LIKE '%@gmail.com';

SELECT name, price
FROM book
WHERE price BETWEEN 20.00::money AND 30.00::money;

SELECT id, total_price
FROM orders
WHERE total_price > 45.00::money;

--find all books names, the number of which is more than 10
SELECT b.name, i.quantity_in_stock
FROM book AS b
JOIN inventory AS i
ON b.inventory_id = i.id
WHERE quantity_in_stock > 10;

--find orders over 50.00
SELECT o.id, o.date, p.username
FROM orders AS o
JOIN profile AS p
ON o.profile_id = p.id
WHERE o.sub_total_price > 50.00::money;

--find all books from publishers 'McGraw-Hill' and 'O Reilly Media'
SELECT b.name, b.price, p.name 
FROM book AS b
JOIN publisher AS p
ON b.publisher_id = p.id
WHERE p.name IN ('McGraw-Hill', 'O Reilly Media');

SELECT first_name, last_name
FROM author
WHERE address IS NULL;

SELECT first_name, last_name
FROM author
WHERE address IS NOT NULL;

--find name and price of books from publisher 'McGraw-Hill'
SELECT name, price
FROM book
WHERE publisher_id=(SELECT id 
				   FROM publisher
				   WHERE name='McGraw-Hill');

--find name and price of books from author with lastname 'Sierra' 
SELECT name, price
FROM book
WHERE author_id=(SELECT id 
				   FROM author
				   WHERE last_name='Sierra');

SELECT name, price
FROM book
ORDER BY price ASC;

--find user orders sorted by date
SELECT o.date, p.username
FROM orders AS o
JOIN profile AS p
ON o.profile_id = p.id
ORDER BY o.date ASC;

--show titles of books and authors sorted by price starting from most expensive
SELECT b.name, b.price, a.first_name, a.last_name
FROM book AS b
INNER JOIN author AS a
ON b.author_id = a.id
ORDER BY b.price DESC;

--select books with titles containing 'head first', as well as books over 30.00
SELECT name, price
FROM book
WHERE lower(name) LIKE '%head first%'
UNION
SELECT name, price
FROM book
WHERE price > 30.00::money;

--select profiles with role 'admin'
SELECT p.username, p.email
FROM profile AS p
JOIN profile_role AS pr
ON pr.profile_id = p.id
JOIN role AS r
ON r.id = pr.role_id
WHERE r.name = 'admin';

--select books and their order dates
SELECT o.date, b.name
FROM orders AS o
JOIN order_book AS ob
ON ob.order_id = o.id
JOIN book AS b
ON b.id = ob.book_id;

--select profiles and their roles
SELECT p.username, r.name
FROM profile AS p
JOIN profile_role AS pr
ON pr.profile_id = p.id
JOIN role AS r
ON r.id = pr.role_id;

--select the ordered books, their quantity and order amount for each profile
SELECT p.username, b.name, ob.quantity, ob.price
FROM profile AS p
LEFT JOIN orders AS o
ON o.profile_id = p.id
LEFT JOIN order_book AS ob
ON o.id = ob.order_id
LEFT JOIN book AS b
ON ob.book_id = b.id;

--select the titles of books, their quantity in stock and the usernames of customers
SELECT b.name AS book_name, i.quantity_in_stock, p.username
FROM inventory AS i
RIGHT JOIN book AS b
ON b.inventory_id = i.id
RIGHT JOIN order_book AS ob
ON ob.book_id = b.id
RIGHT JOIN orders AS o
ON o.id = ob.order_id
RIGHT JOIN profile AS p
ON p.id = o.profile_id;

SELECT b.name, ROUND(AVG(ob.price::numeric), 2) average_price_in_order
FROM book AS b
JOIN order_book AS ob
ON ob.book_id = b.id
GROUP BY b.name;

SELECT name, price, ROUND(SQRT(price::numeric), 2) sqrt_of_price
FROM book;

SELECT username, age(birthday) age
FROM profile;

SELECT name, price
FROM book
GROUP BY name, price
HAVING lower(name) LIKE '%head first%';

SELECT username, email, position('@' in email)
FROM profile;

SELECT MIN(price)
FROM book;

--count and display the total number of ordered books for each book
SELECT b.name, SUM(ob.quantity) ordered_quantity
FROM book AS b
JOIN order_book AS ob
ON ob.book_id = b.id
GROUP BY b.name;

--select the order amount without delivery for users with email gmail.com
SELECT p.email, SUM(ob.price) total_price
FROM profile AS p
JOIN orders AS o
ON o.profile_id = p.id
JOIN order_book AS ob
ON ob.order_id = o.id
GROUP BY p.email
HAVING lower(p.email) LIKE '%@gmail.com';















