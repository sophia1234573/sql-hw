INSERT INTO author (first_name, last_name, address)
VALUES ('Cay', 'Horstmann', 'USA');

INSERT INTO author (first_name, last_name, address)
VALUES ('Joshua', 'Bloch', 'USA');

INSERT INTO author (first_name, last_name, address)
VALUES ('Herbert', 'Schildt', 'USA');

INSERT INTO author (first_name, last_name, address)
VALUES ('Kathy', 'Sierra', 'USA');

INSERT INTO author (first_name, last_name, address)
VALUES ('Brian', 'Goetz', 'USA');

INSERT INTO author (first_name, last_name, address)
VALUES ('Lasse', 'Koskela', 'Finland');

INSERT INTO author (first_name, last_name, address)
VALUES ('Brett', 'McLaughlin', 'USA');

INSERT INTO author (first_name, last_name, address)
VALUES ('Eric', 'Freeman', 'USA');

INSERT INTO author (first_name, last_name, address)
VALUES ('Scott', 'Oaks', 'USA');

INSERT INTO publisher (name, address)
VALUES ('Prentice Hall', 'savvas.com');

INSERT INTO publisher (name, address)
VALUES ('Addison-Wesley Professional', 'pearsonhighered.com');

INSERT INTO publisher (name, address)
VALUES ('McGraw-Hill', 'mheducation.com');

INSERT INTO publisher (name, address)
VALUES ('O Reilly Media', 'oreilly.com');

INSERT INTO publisher (name, address)
VALUES ('Manning Publications', 'https://www.linkedin.com/company/manning-publications-co/');

INSERT INTO inventory (quantity_in_stock)
VALUES (10);

INSERT INTO inventory (quantity_in_stock)
VALUES (8);

INSERT INTO inventory (quantity_in_stock)
VALUES (15);

INSERT INTO inventory (quantity_in_stock)
VALUES (11);

INSERT INTO inventory (quantity_in_stock)
VALUES (14);

INSERT INTO inventory (quantity_in_stock)
VALUES (11);

INSERT INTO inventory (quantity_in_stock)
VALUES (6);

INSERT INTO inventory (quantity_in_stock)
VALUES (12);

INSERT INTO inventory (quantity_in_stock)
VALUES (13);

INSERT INTO inventory (quantity_in_stock)
VALUES (14);

INSERT INTO book (name, price, link_to_photo, author_id, publisher_id, inventory_id)
VALUES ('Core Java Volume I � Fundamentals', 25.25, 'some link to photo 1', 1, 1, 1);

INSERT INTO book (name, price, link_to_photo, author_id, publisher_id, inventory_id)
VALUES ('Effective Java', 37.45, 'some link to photo 2', 2, 2, 2);

INSERT INTO book (name, price, link_to_photo, author_id, publisher_id, inventory_id)
VALUES ('Java: A Beginner�s Guide', 19.99, 'some link to photo 3', 3, 3, 3);

INSERT INTO book (name, price, link_to_photo, author_id, publisher_id, inventory_id)
VALUES ('Java - The Complete Reference', 21.25, 'some link to photo 4', 3, 3, 4);

INSERT INTO book (name, price, link_to_photo, author_id, publisher_id, inventory_id)
VALUES ('Head First Java', 20.25, 'some link to photo 5', 4, 4, 5);

INSERT INTO book (name, price, link_to_photo, author_id, publisher_id, inventory_id)
VALUES ('Java Concurrency in Practice', 15.25, 'some link to photo 6', 5, 2, 6);

INSERT INTO book (name, price, link_to_photo, author_id, publisher_id, inventory_id)
VALUES ('Test-Driven: TDD and Acceptance TDD for Java Developers', 29.30, 'some link to photo 7', 6, 5, 7);

INSERT INTO book (name, price, link_to_photo, author_id, publisher_id, inventory_id)
VALUES ('Head First Object-Oriented Analysis Design', 20.35, 'some link to photo 8', 7, 4, 8);

INSERT INTO book (name, price, link_to_photo, author_id, publisher_id, inventory_id)
VALUES ('Java Performance: The Definite Guide', 45.15, 'some link to photo 9', 9, 4, 9);

INSERT INTO book (name, price, link_to_photo, author_id, publisher_id, inventory_id)
VALUES ('Head First Design Patterns', 25.75, 'some link to photo 10', 9, 4, 10);

INSERT INTO profile (username, password, email, birthday, gender)
VALUES ('abcd', 'password', 'kfjjjgjld@gmail.com', '1985-01-23', 'male');

INSERT INTO profile (username, password, email, birthday, gender)
VALUES ('lakjdj', 'verysecretpassword', 'gdhd838hd@gmail.com', '1985-10-25', 'female');

INSERT INTO profile (username, password, email, birthday, gender)
VALUES ('[skkjsh', '12345', 'jfllwifnfn@gmail.com', '1995-03-05', 'male');

INSERT INTO profile (username, password, email, birthday, gender)
VALUES ('iiisjj', 'allldjw', 'pqppeooe123@gmail.com', '2000-11-11', 'female');

INSERT INTO profile (username, password, email, birthday, gender)
VALUES ('hgfd', 'secretpassword', 'mkdkkkkf@ukr.net', '1981-01-01', 'male');

INSERT INTO profile (username, password, email, birthday, gender)
VALUES ('plqirnn', 'password12345', 'pwpwpw@gmail.com', '1986-03-14', 'female');

INSERT INTO profile (username, password, email, birthday, gender)
VALUES ('sophia', '1234567', 'sophia@gmail.com', '1987-05-28', 'female');

INSERT INTO profile (username, password, email, birthday, gender)
VALUES ('kolya', '98765', 'kolya@ukr.net', '2011-02-03', 'male');

INSERT INTO profile (username, password, email, birthday, gender)
VALUES ('sveta', '67584930', 'sveta@gmail.com', '1994-09-03', 'female');

INSERT INTO role (name)
VALUES ('user');

INSERT INTO role (name)
VALUES ('admin');

INSERT INTO orders (date, profile_id, sub_total_price, delivery_price, delivery_type, status, total_price)
VALUES ('2021-07-02 9:15:45', 2, 25.25, 0.00, 'pickup', 'registered', 25.25);

INSERT INTO orders (date, profile_id, sub_total_price, delivery_price, delivery_type, status, total_price)
VALUES ('2021-07-04 10:13:54', 6, 49.65, 0.00, 'pickup', 'registered', 49.65);

INSERT INTO orders (date, profile_id, sub_total_price, delivery_price, delivery_type, status, total_price)
VALUES ('2021-07-04 12:41:39', 5, 61.23, 0.00, 'pickup', 'registered', 61.23);

INSERT INTO orders (date, profile_id, sub_total_price, delivery_price, delivery_type, status, total_price)
VALUES ('2021-07-05 15:20:14', 9, 65.40, 0.00, 'pickup', 'registered', 65.40);

INSERT INTO orders (date, profile_id, sub_total_price, delivery_price, delivery_type, status, total_price)
VALUES ('2021-07-06 11:05:25', 1, 15.25, 0.00, 'pickup', 'registered', 15.25);

INSERT INTO orders (date, profile_id, sub_total_price, delivery_price, delivery_type, status, total_price)
VALUES ('2021-07-06 12:24:12', 3, 21.25, 0.00, 'pickup', 'registered', 21.25);

INSERT INTO orders (date, profile_id, sub_total_price, delivery_price, delivery_type, status, total_price)
VALUES ('2021-07-06 16:42:25', 8, 25.75, 0.00, 'pickup', 'registered', 25.75);

INSERT INTO orders (date, profile_id, sub_total_price, delivery_price, delivery_type, status, total_price)
VALUES ('2021-07-07 9:01:03', 7, 60.25, 0.00, 'pickup', 'registered', 60.25);


INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (1, 1, 1, 20.25);

INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (2, 7, 1, 29.30);

INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (2, 8, 1, 20.35);

INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (3, 4, 1, 21.25);

INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (3, 3, 2, 39.98);

INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (4, 5, 1, 20.25);

INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (4, 9, 1, 45.15);

INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (5, 6, 1, 15.25);

INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (6, 7, 1, 21.25);

INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (7, 10, 1, 25.75);

INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (8, 6, 2, 30.50);

INSERT INTO order_book (order_id, book_id, quantity, price)
VALUES (8, 7, 1, 29.30);

INSERT INTO profile_role (profile_id, role_id)
VALUES (1, 1);

INSERT INTO profile_role (profile_id, role_id)
VALUES (2, 1);

INSERT INTO profile_role (profile_id, role_id)
VALUES (3, 1);

INSERT INTO profile_role (profile_id, role_id)
VALUES (4, 2);

INSERT INTO profile_role (profile_id, role_id)
VALUES (5, 1);

INSERT INTO profile_role (profile_id, role_id)
VALUES (6, 1);

INSERT INTO profile_role (profile_id, role_id)
VALUES (7, 1);

INSERT INTO profile_role (profile_id, role_id)
VALUES (8, 1);

INSERT INTO profile_role (profile_id, role_id)
VALUES (9, 1);

UPDATE inventory
SET quantity_in_stock=10
WHERE id=(SELECT inventory_id 
		 FROM book
		 WHERE name='Head First Java');

UPDATE profile
SET username='starwars'
WHERE email='pqppeooe123@gmail.com';

DELETE FROM order_book
WHERE order_id=
   (SELECT id
    FROM orders
    WHERE profile_id=
       (SELECT id 
	    FROM profile
	    WHERE username='hgfd')
   );

UPDATE author
SET address=NULL
WHERE first_name='Scott';
















