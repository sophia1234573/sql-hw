CREATE TABLE IF NOT EXISTS profile
(
	id SERIAL PRIMARY KEY,
	username VARCHAR(256) NOT NULL,
	password VARCHAR(256) NOT NULL,
	email VARCHAR(256) NOT NULL,
	birthday DATE NOT NULL,
	gender VARCHAR(10) NOT NULL
);

CREATE TABLE IF NOT EXISTS role
(
	id SERIAL PRIMARY KEY,
	name VARCHAR(256) NOT NULL
);

CREATE TABLE IF NOT EXISTS author
(
	id SERIAL PRIMARY KEY,
	first_name VARCHAR(256) NOT NULL,
	last_name VARCHAR(256) NOT NULL,
	address VARCHAR(256)  
);

CREATE TABLE IF NOT EXISTS publisher
(
	id SERIAL PRIMARY KEY,
	name VARCHAR(256) NOT NULL,
	address VARCHAR(256)  
);

CREATE TABLE IF NOT EXISTS inventory
(
	id SERIAL PRIMARY KEY,
	quantity_in_stock INT NOT NULL
);

CREATE TABLE IF NOT EXISTS book
(
	id SERIAL PRIMARY KEY,
	name VARCHAR(256) NOT NULL,
	price MONEY NOT NULL,
	link_to_photo VARCHAR(256),
	visible BOOLEAN DEFAULT TRUE,
	author_id INT,
	FOREIGN KEY (author_id) REFERENCES author (id),
	publisher_id INT,
	FOREIGN KEY (publisher_id) REFERENCES publisher (id),
	inventory_id INT,
	FOREIGN KEY (inventory_id) REFERENCES inventory (id)
);

CREATE TABLE IF NOT EXISTS orders
(
	id SERIAL PRIMARY KEY,
	order_date TIMESTAMP NOT NULL,
	profile_id INT,
	FOREIGN KEY (profile_id) REFERENCES profile (id),
	sub_total_price money NOT NULL,
	delivery_price money NOT NULL,
	delivery_type VARCHAR(256),
	status VARCHAR(256) NOT NULL,
	total_price money NOT NULL
);

CREATE TABLE IF NOT EXISTS order_book
(
	id SERIAL PRIMARY KEY,
	order_id INT,
	FOREIGN KEY (order_id) REFERENCES orders (id),
	book_id INT,
	FOREIGN KEY (book_id) REFERENCES book (id),
	quantity INT NOT NULL,
	price money NOT NULL
);

CREATE TABLE IF NOT EXISTS profile_role
(
	id SERIAL PRIMARY KEY,
	profile_id INT,
	FOREIGN KEY (profile_id) REFERENCES profile (id),
	role_id INT,
	FOREIGN KEY (role_id) REFERENCES role (id)
);

ALTER TABLE profile
ADD UNIQUE(email);

ALTER TABLE orders
RENAME COLUMN order_date TO date;

ALTER TABLE orders
ALTER COLUMN delivery_price 
DROP NOT NULL;

ALTER TABLE orders
ALTER COLUMN delivery_price 
SET DEFAULT 0.00;

ALTER TABLE orders
ALTER COLUMN delivery_type 
DROP NOT NULL;

ALTER TABLE orders
ALTER COLUMN delivery_type
SET DEFAULT 'pickup';

ALTER TABLE profile
ALTER COLUMN birthday TYPE timestamp;


