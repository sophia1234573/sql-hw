CREATE INDEX book_name
ON book(name);

CREATE INDEX email
ON profile(email);

CREATE SEQUENCE book_seq
INCREMENT 10
MINVALUE 10
START WITH 10;



